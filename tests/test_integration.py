from pathlib import Path
from typing import Any, Dict, List

import pytest

from sudokuAI.sudoku_lightning import train


class TestTraining:

    @pytest.fixture()
    def yaml_overloads(self) -> List[Path]:
        return ["config/test.yaml"]

    @pytest.fixture()
    def kwarg_overloads(self) -> Dict[str, Any]:
        return {"trainer": {"fast_dev_run": 1}}

    def test_fast_dev_run(self, yaml_overloads, kwarg_overloads):
        output = train.run(
            yaml_overload=yaml_overloads,
            kwarg_overload=kwarg_overloads,
        )
        assert output is None

    def test_most_basic(self, yaml_overloads):
        output = train.run(yaml_overload=yaml_overloads, kwarg_overload={})
        assert output > 0.0

    def test_log_folder_is_created(
        self, yaml_overloads, kwarg_overloads, tmp_path: Path
    ):
        kwarg_overloads["logdir"] = tmp_path / "logs"
        assert len(list(tmp_path.glob("*"))) == 0, "tmpdir is not empty"
        train.run(yaml_overload=yaml_overloads, kwarg_overload=kwarg_overloads)
        assert len(list(tmp_path.glob("*"))) > 0
