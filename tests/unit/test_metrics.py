# Basic Imports


# External Imports
import numpy as np
import pytest
import torch

# Internal Imports
from sudokuAI.sudoku_lightning.metrics import Accuracy

# Global Variables
# n/a


@pytest.fixture(params=[torch.ones(9, 9, 10)])
def target(request):
    yield request.param.long()


@pytest.fixture(params=[0, 0.3, 0.5, 1.0])
def proportion(request):
    yield request.param


@pytest.fixture()
def incorrect(target, proportion):
    indexes = np.random.choice(range(target.numel()), int(proportion * target.numel()))
    val = target.clone()

    val.flatten()[indexes] = 0
    val = val.view(target.shape)
    yield val


def test_accuracy(target, incorrect, proportion):
    assert (
        Accuracy(reduction="mean")(incorrect, target) == incorrect.sum() / target.sum()
    )
