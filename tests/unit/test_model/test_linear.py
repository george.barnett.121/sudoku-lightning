# Copyright George Barnett 2020

"""data loaindg

Basic class for training and evaluating model
"""

# Basic Imports
# n/a

import numpy as np
import pytest

# External Imports
import torch

# Internal Imports
from sudokuAI.sudoku_lightning.model import BasicLinear

# Global Variables
# n/a


@pytest.fixture(params=[2, 20])
def batch_size(request) -> int:
    yield request.param


@pytest.fixture(params=[9])
def puzzle_size(request) -> int:
    yield request.param


@pytest.fixture()
def example_data(batch_size: int, puzzle_size: int) -> torch.Tensor:
    arr = np.arange(batch_size * puzzle_size * puzzle_size).reshape(
        (batch_size, puzzle_size, puzzle_size)
    )
    yield torch.tensor(arr, dtype=torch.float) / arr.max()


class TestBasicLinear:
    @pytest.fixture(params=[5, 20])
    def num_layers(self, request) -> int:
        yield request.param

    @pytest.fixture()
    def num_features(self, puzzle_size, num_layers) -> int:
        return [puzzle_size**2] * num_layers

    @pytest.fixture(params=[True, False])
    def skip_connection(self, request) -> bool:
        yield request.param

    @pytest.fixture(params=[True, False])
    def batch_norm(self, request) -> bool:
        yield request.param

    def test_layers(self, puzzle_size, num_features: int):
        model = BasicLinear(num_features=num_features)
        assert len(model.body) == len(num_features) - 2
        assert model.head

    def test_no_layers(
        self,
    ):
        with pytest.raises(ValueError):
            BasicLinear(num_features=[])

    @pytest.fixture()
    def basic_model(self, num_features, skip_connection, batch_norm) -> BasicLinear:
        yield BasicLinear(
            num_features=num_features, skip=skip_connection, batch_norm=batch_norm
        )

    def test_forward(
        self,
        basic_model: BasicLinear,
        batch_norm: bool,
        batch_size: int,
        example_data: torch.Tensor,
    ):
        prediction, output = basic_model(example_data)
        assert output.shape == example_data.shape
        assert prediction.shape == example_data.shape

    @pytest.mark.parametrize("batch_size,batch_norm", [(1, False), (1, True)])
    # Overrides the fixture parameter batch_size to set as 1
    # effects example_model too
    def test_forward_bs_1(
        self, batch_size, example_data, batch_norm, num_features, basic_model
    ):
        """Edge case of batch size of 1

        If there is 1d batchnorms, then the batch size cannot be 1.
        """

        # The last layer is always set to have no batchnorm
        # So just check shape
        if num_features == 1:
            output = basic_model(example_data)
            assert output.shape == example_data.shape
        # If batch_size is 1, and batchnorm is on, we should raise an error
        elif batch_size == 1 and batch_norm:
            with pytest.raises(ValueError):
                basic_model(example_data)
