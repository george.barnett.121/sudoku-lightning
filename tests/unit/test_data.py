# Copyright George Barnett 2020

"""data loaindg

Basic class for training and evaluating model
"""

# Basic Imports
from pathlib import Path
from typing import Tuple

import h5py

# External Imports
import numpy as np
import pytest

# Internal Imports
from sudokuAI.sudoku_lightning.data import SodokuPairsDataset

# Global Variables
# n/a


@pytest.fixture
def example_data() -> Tuple[np.ndarray, np.ndarray]:
    p = np.arange(4 * 3).reshape((3, 2, 2))
    s = p * 10
    return (p, s)


@pytest.fixture
def h5py_path(example_data, tmp_path) -> Path:
    save_path = tmp_path / "test.hdf5"
    p, s = example_data
    hf = h5py.File(save_path, "w")
    hf.create_dataset("problems", data=p)
    hf.create_dataset("solutions", data=s)
    hf.close()
    yield save_path
    # File should not be deleted during testing
    save_path.unlink(missing_ok=False)


# TEST sodokuDataset
def test_loading(h5py_path):
    dataset = SodokuPairsDataset(h5py_path)
    assert hasattr(dataset, "problems")
    assert hasattr(dataset, "solutions")


@pytest.fixture
def example_dataset(h5py_path):
    # This may fail if test_loading fails
    yield SodokuPairsDataset(h5py_path)


def test_len(
    example_dataset: SodokuPairsDataset, example_data: Tuple[np.ndarray, np.ndarray]
):
    problem, solution = example_data
    assert len(example_dataset) == problem.shape[0]


def test_get_item(
    example_dataset: SodokuPairsDataset, example_data: Tuple[np.ndarray, np.ndarray]
):
    problem, solution = example_data
    test_prob, test_sol = example_dataset[0]
    assert np.array_equal(problem[0], test_prob)
    assert np.array_equal(solution[0], test_sol)
