"""Data Loading

Basic class for training and evaluating model
"""

# Basic Imports
from pathlib import Path
from typing import Tuple

import h5py
import numpy as np

# External Imports
import torch
from torch.utils.data.dataset import Dataset
from torchvision import transforms

# dataset is assumed from here:
# https://www.kaggle.com/bryanpark/sudoku


class SodokuPairsDataset(Dataset):
    def __init__(self, data_path: Path, cache=False):
        """Dataset for laoding sodoku games

        Load a csv of 9x9 problems and solutions. The problems take the form of
        ints in the csv, and so must be converted to numpy int arrays.

        Args:
            data_path: Path to h5py file
        """
        data_file = h5py.File(data_path, "r")
        self.problems = data_file["problems"]
        self.solutions = data_file["solutions"]
        self.cache = cache
        if cache:
            self.problems = np.array(self.problems)
            self.solutions = np.array(self.solutions)

        self.classes = np.unique(self.solutions[0])

        self.transforms = transforms.Compose(
            [
                transforms.ToTensor(),
                transforms.Lambda(self._squeeze_batch),
                transforms.Lambda(self._to_float),
            ]
        )

    def _squeeze_batch(self, x: torch.Tensor) -> torch.Tensor:
        return x.squeeze(0)

    def _to_float(self, x: torch.Tensor) -> torch.Tensor:
        return x.float()

    def __len__(self) -> int:
        # return len(self.game_pairs)
        assert len(self.problems) == len(self.solutions)
        return len(self.problems)

    def __getitem__(self, item_idx: int) -> Tuple[np.ndarray, np.ndarray]:
        problem = torch.Tensor(self.problems[item_idx])
        solution = torch.Tensor(self.solutions[item_idx])

        return problem, solution
