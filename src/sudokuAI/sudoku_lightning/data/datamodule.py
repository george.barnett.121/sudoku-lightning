"""Data Loading

Basic class for training and evaluating model
"""

# Basic Imports
import os
from typing import Optional

# External Imports
from pytorch_lightning import LightningDataModule
from torch.utils.data import DataLoader

# Internal Imports
from ..config import DatasetConfig
from .dataset import SodokuPairsDataset


class LitSudokuDataMododule(LightningDataModule):
    def __init__(self, config: Optional[DatasetConfig] = None):
        super().__init__()
        config = DatasetConfig() if config is None else config
        self.save_hyperparameters(config.dict())

        self.train_path = config.train_path
        self.val_path = config.val_path
        self.test_path = config.test_path

        self.cache = config.cache
        self.num_workers = config.workers if config.workers else os.cpu_count()
        self.persistent_workers = config.persistent_workers
        self.batch_size = config.batch_size

    # ---------------------------------------------------------------------#
    # --- Functions Specifically called as part of LightningDatamodule --- #
    # ---------------------------------------------------------------------#
    def prepare_data(self):
        # called only on 1 GPU
        pass

    def setup(self, stage: Optional[str] = None):
        # called on every GPU
        self.train, self.val, self.test = self.load_datasets()

    def train_dataloader(self):
        return DataLoader(
            self.train,
            num_workers=self.num_workers,
            batch_size=self.batch_size,
            pin_memory=True,
            shuffle=True,
            persistent_workers=self.persistent_workers,
        )

    def val_dataloader(self):
        return DataLoader(
            self.val,
            num_workers=self.num_workers,
            batch_size=self.batch_size,
            pin_memory=True,
            shuffle=False,
            persistent_workers=self.persistent_workers,
        )

    def test_dataloader(self):
        return DataLoader(
            self.test,
            num_workers=self.num_workers,
            batch_size=self.batch_size,
            pin_memory=True,
            shuffle=False,
            persistent_workers=self.persistent_workers,
        )

    # --------------------------------#
    # --- Custom Helper Functions --- #
    # --------------------------------#
    def load_datasets(self):
        return self.train_dataset(), self.val_dataset(), self.test_dataset()

    def train_dataset(self):
        """Load Train Dataset

        Not required for pytorch lightning
        """
        # data
        return SodokuPairsDataset(self.train_path, self.cache)

    def val_dataset(self):
        return SodokuPairsDataset(self.val_path, self.cache)

    def test_dataset(self):
        if self.test_path is None:
            return None
        else:
            return SodokuPairsDataset(self.test_path, self.cache)
