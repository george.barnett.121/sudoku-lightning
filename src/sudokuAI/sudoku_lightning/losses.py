# Copyright George Barnett 2020

"""Configs
"""

# Basic Imports

# External Imports
import torch
from torch import nn

# Internal Imports
# N/A


class CrossEntropyLoss(nn.CrossEntropyLoss):
    def forward(self, input, target):
        return super(CrossEntropyLoss, self).forward(input, target.type(torch.long))
