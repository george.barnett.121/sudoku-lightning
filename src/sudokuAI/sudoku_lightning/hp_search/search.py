"""
This script runs hyperparameter optimization using the Hyperopt library with a
custom search space defined in an external Python file. The script dynamically
loads the search space and runs a user-defined number of trials using Tree of
Parzen Estimators (TPE) to suggest hyperparameters. The results of the trials
are saved to a specified file for later analysis.

Usage:
    python <script_name>.py
    --space-file <path_to_search_space_file>
    [--trial-dir <directory_to_save_trials>]

Example:
    python optimize.py --space-file search_space.py --trial-dir trials
"""

import argparse
import importlib.util
import logging
from pathlib import Path

from hyperopt import fmin, space_eval, tpe

from sudokuAI.sudoku_lightning.train import run

# Global Variables
logger = logging.getLogger(__name__)

# Configure logging to output to the console
logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(name)s - %(levelname)s - %(message)s"
)


def objective(param_dict: dict) -> float:
    """
    Defines the objective function to minimize using the provided hyperparameter
    configuration.

    Args:
        param_dict (dict): Dictionary containing hyperparameter values to be
            used for training.

    Returns:
        float: The negative of the evaluation score from the model training.
    """
    logger.info("Hyper Parameter Objective Function: Starting")
    out = -1 * run(
        kwarg_overload=param_dict["kwarg_overload"],
        yaml_overload=param_dict["yaml_overload"],
    )
    logger.info("Hyper Parameter Objective Function: Finished")
    return out


def import_search_space_from_file(file_path: str) -> dict:
    """
    Dynamically imports a Python module containing the search space from the
    specified file path.

    The module is expected to define a variable `space` which contains the
    search space dictionary for hyperparameter optimization.

    Args:
        file_path (str): The file path to the Python module defining the search
            space.

    Returns:
        dict: The search space dictionary.

    Raises:
        AttributeError: If the specified file does not define a variable `space`.
        ValueError: If the search space is empty.
    """
    spec = importlib.util.spec_from_file_location("search_space", file_path)
    search_space_module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(search_space_module)

    if hasattr(search_space_module, "space"):
        space = search_space_module.space
        if not space:
            raise ValueError("The search space cannot be empty.")
        return space
    else:
        raise AttributeError(
            f"The file {file_path} does not define a variable `space`."
        )


def run_hyperopt(space_file: str, trial_dir: str) -> None:
    """
    Runs the Hyperopt optimization process.

    Args:
        space_file (str): Path to the Python file defining the search space.
        trial_dir (str): Directory to save the trial results.
    """
    # Load the search space from the provided Python file
    space = import_search_space_from_file(space_file)

    # Check that the search space contains 'exp_name'
    if "exp_name" not in space:
        raise KeyError("The search space file must define an 'exp_name' key.")
    exp_name = space["exp_name"]
    num_trials = space["num_trials"]

    # Define the path for saving trial results
    trial_path = Path(f"{trial_dir}/{exp_name}.txt")
    trial_path.parent.mkdir(parents=True, exist_ok=True)

    # Run Hyperopt to minimize the objective function
    best = fmin(
        objective,
        space,
        algo=tpe.suggest,
        max_evals=num_trials,
        show_progressbar=False,
        trials_save_file=trial_path,
    )

    # Print the best hyperparameter set and its evaluated result
    print(best)
    print(space_eval(space, best))


def cli():
    """Main entry point of the script.

    Sets up command-line argument parsing, loads the search space, and runs
    hyperparameter optimization using the specified number of trials.
    """
    # Set up argparse to handle the path to the search space file and other parameters
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "--space-file",
        type=str,
        required=True,
        help="Path to the Python file defining the search space",
    )
    parser.add_argument(
        "--trial-dir",
        type=str,
        default="trials",
        help="Directory to save trial results",
    )

    args = parser.parse_args()

    # Run the hyperparameter optimization process
    run_hyperopt(args.space_file, args.trial_dir)


if __name__ == "__main__":
    cli()
