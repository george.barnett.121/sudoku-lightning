""" Basic Training script"""

# Basic Imports
import argparse
import hashlib
import json
import logging
import sys
from pathlib import Path
from typing import Any, Dict, Iterable, Literal, Union

# External Imports
import pytorch_lightning as pl
import yaml
from omegaconf import OmegaConf
from pydantic import BaseModel
from pytorch_lightning import callbacks

# Internal Imports
from sudokuAI.sudoku_lightning import callbacks as my_callbacks
from sudokuAI.sudoku_lightning import config as prj_configs
from sudokuAI.sudoku_lightning.callbacks.wandb_logger import FinishableWandbLogger
from sudokuAI.sudoku_lightning.data import LitSudokuDataMododule
from sudokuAI.sudoku_lightning.model import LitLinearSudoku

# Global Variables
_logger = logging.getLogger(__name__)


def train_model(config: prj_configs.ExpConfig):
    # exp_name is based on hash if nothing is provided
    version = config.version if config.version else _hash_config(config)

    full_log_path = Path(config.logdir) / config.exp_name / version
    full_log_path.mkdir(parents=True, exist_ok=True)
    with open(full_log_path / "exp.yaml", "w") as f:
        yaml.dump(config.dict(), f)

    # Configure data
    sudoku_data_module = LitSudokuDataMododule(
        config=config.dataset,
    )

    # Configure Loggers
    loggers = []
    for logger_cfg in config.loggers:
        if isinstance(logger_cfg, prj_configs.TensorBoardLoggerConfig):
            loggers.append(
                pl.loggers.TensorBoardLogger(
                    save_dir=config.logdir,
                    name=config.exp_name,
                    version=version,
                    sub_dir="tb",
                    log_graph=logger_cfg.log_graph,
                )
            )
        if isinstance(logger_cfg, prj_configs.CSVLoggerConfig):
            loggers.append(
                pl.loggers.CSVLogger(
                    save_dir=config.logdir, name=config.exp_name, version=version
                )
            )
        if isinstance(logger_cfg, prj_configs.WandbLoggerConfig):
            loggers.append(
                FinishableWandbLogger(
                    project="sudoku_lightning",
                    save_dir=config.logdir,
                    name=f"{config.exp_name}/{version}",
                    log_model=logger_cfg.log_model,
                    mode=logger_cfg.mode,
                )
            )
    # Model
    model = LitLinearSudoku(config=config.litmodule)

    # Callbacks

    checkpoint_callback = callbacks.ModelCheckpoint(
        monitor=model.val_metric_str,
        dirpath=full_log_path / "checkpoints",
        filename="-".join(
            [
                "epoch={epoch:02d}",
                f"{model.val_metric_str.replace('/', '_')}"
                + "={"
                + f"{model.val_metric_str}"
                + ":.3f}",
            ]
        ),
        auto_insert_metric_name=False,
        save_top_k=5,
        mode=model.val_metric_direction,
        save_last=True,
        every_n_epochs=1,
        verbose=True,
    )

    exp_callbacks = [checkpoint_callback]

    for callback_cfg in config.callbacks:
        if isinstance(callback_cfg, prj_configs.LearningRateMonitorConfig):
            exp_callbacks.append(
                callbacks.LearningRateMonitor(
                    logging_interval=callback_cfg.logging_interval,
                    log_momentum=callback_cfg.log_momentum,
                )
            )
        elif isinstance(callback_cfg, prj_configs.PerformanceThresholdCallbackConfig):
            exp_callbacks.append(
                my_callbacks.PerformanceThresholds(
                    monitor=(
                        callback_cfg.monitor
                        if callback_cfg.monitor is not None
                        else model.val_metric_str
                    ),
                    thresholds=callback_cfg.thresholds,
                    mode=(
                        model.val_metric_direction
                        if callback_cfg.mode is None
                        else callback_cfg.mode
                    ),
                    verbose=callback_cfg.verbose,
                    log_rank_zero_only=callback_cfg.log_rank_zero_only,
                )
            )
        elif isinstance(callback_cfg, prj_configs.StochasticWeightAveragingConfig):
            exp_callbacks.append(
                callbacks.StochasticWeightAveraging(
                    swa_lrs=(
                        callback_cfg.swa_lrs
                        if callback_cfg.swa_lrs is not None
                        else config.litmodule.lr
                    ),
                )
            )
        elif isinstance(callback_cfg, prj_configs.EarlyStoppingConfig):
            exp_callbacks.append(
                callbacks.EarlyStopping(
                    monitor=(
                        callback_cfg.monitor
                        if callback_cfg.monitor is not None
                        else model.val_metric_str
                    ),
                    mode=(
                        model.val_metric_direction
                        if callback_cfg.mode is None
                        else callback_cfg.mode
                    ),
                    patience=callback_cfg.patience,
                    check_finite=callback_cfg.check_finite,
                    min_delta=callback_cfg.min_delta,
                    strict=callback_cfg.strict,
                )
            )

    # Define Trainer
    trainer = pl.Trainer(
        default_root_dir=full_log_path,
        logger=loggers,
        callbacks=exp_callbacks,
        **config.trainer.model_dump(),
    )

    # Check if there is a checkpoint to resume from there if so
    last_path = Path(checkpoint_callback.dirpath) / "last.ckpt"
    resuming_training: bool = last_path.exists()

    trainer.fit(
        model=model,
        datamodule=sudoku_data_module,
        ckpt_path=last_path if resuming_training else None,
    )
    _logger.info("Finializing loggers...")
    for pl_logger in loggers:
        pl_logger.finalize(status="success")
    _logger.info("Finished loggers")

    if checkpoint_callback.best_model_score is None:
        return
    return checkpoint_callback.best_model_score.item()


# ---- CLI ----
# The functions defined in this section are wrappers around the main Python
# API allowing them to be called directly from the terminal as a CLI
# executable/script.


def parse_args(args):
    """Parse command line parameters

    Args:
      args (List[str]): command line parameters as list of strings
          (for example  ``["--help"]``).

    Returns:
      :obj:`argparse.Namespace`: command line parameters namespace
    """
    parser = argparse.ArgumentParser(description="Just a Fibonacci demonstration")
    parser.add_argument(
        "-e",
        "--exp-yamls",
        type=Path,
        nargs="+",
        default=[],
        required=False,
    )
    parser.add_argument(
        "-o",
        "--overload",
        type=str,
        nargs="+",
        required=False,
    )
    parser.add_argument(
        "-v",
        "--verbose",
        help="set loglevel to INFO",
        action="store_true",
    )
    parser.add_argument(
        "-vv",
        "--debug",
        help="set loglevel to DEBUG",
        action="store_true",
    )
    return parser.parse_args(args)


def setup_logging(verbose: bool = True, debug: bool = True):
    """Setup basic logging

    Args:
      loglevel (int): minimum loglevel for emitting messages
    """
    logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    loglevel = logging.ERROR
    if verbose:
        loglevel = logging.INFO
    if debug:
        loglevel = logging.DEBUG
    logging.basicConfig(
        level=loglevel, stream=sys.stdout, format=logformat, datefmt="%Y-%m-%d %H:%M:%S"
    )
    logging.getLogger(__name__).log(level=loglevel, msg=f"{loglevel=}")
    logging.getLogger("pthreadpool").setLevel(logging.ERROR)
    logging.getLogger("fsspec.local").setLevel(logging.INFO)


def construct_config(
    yamls: Iterable[Path] | None,
    cli: argparse.Namespace | None,
    kwargs: Dict[Any, Any] | None,
    args: Iterable[str] | None,
):
    """Create an experiment file from the hierachy

    Use Omegaconf to successively merge/overwrite the default config with configs
    """
    config = {}
    if yamls:
        for yaml_path in yamls:
            yaml_cfg = OmegaConf.load(yaml_path)
            config = OmegaConf.merge(config, yaml_cfg)
    if cli:
        cli_cfg = OmegaConf.from_cli(cli)
        config = OmegaConf.merge(config, cli_cfg)
    if args:
        args_cfg = OmegaConf.from_dotlist(args)
        config = OmegaConf.merge(config, args_cfg)
    if kwargs:
        kwargs_cfg = OmegaConf.create(kwargs)
        config = OmegaConf.merge(config, kwargs_cfg)
    config_dict = OmegaConf.to_container(config, resolve=True)
    # Finally pass through pydantic for validation and to construct the overall config
    _logger.debug(f"Config pre validation: {json.dumps(config_dict, indent=4)}")
    output_cfg = prj_configs.ExpConfig(**config_dict)
    _logger.info(f"Validated Config: {output_cfg.json(indent=4)}")
    return output_cfg


def _hash_config(cfg: BaseModel, mode: Literal["full", "short"] = "short"):
    hex = hashlib.md5(cfg.json().encode()).hexdigest()
    return hex if mode == "full" else hex[:6]


def run(
    yaml_overload: None | Iterable[Path] = None,
    cli_overload: None | argparse.Namespace = None,
    kwarg_overload: None | Dict[Any, Any] = None,
    args_overload: None | Iterable[str] = None,
):
    _logger.info("Constructing config")
    for overload in [yaml_overload, cli_overload, kwarg_overload, args_overload]:
        _logger.debug(f"{overload=}")
    cfg = construct_config(
        yamls=yaml_overload,
        cli=cli_overload,
        kwargs=kwarg_overload,
        args=args_overload,
    )
    setup_logging(verbose=cfg.verbose, debug=cfg.debug)
    _logger.debug(f"Starting Training with {cfg}")
    final_value = train_model(cfg)

    if isinstance(
        final_value,
        Union[
            int,
            float,
        ],
    ):
        final_value = float(final_value)
    _logger.info(f"Final value: {final_value}")
    return final_value


def cli():
    args = sys.argv[1:]
    args = parse_args(args)
    setup_logging(verbose=args.verbose, debug=args.debug)
    return run(
        yaml_overload=args.exp_yamls,
        cli_overload=args.overload,
    )


if __name__ == "__main__":
    cli()
