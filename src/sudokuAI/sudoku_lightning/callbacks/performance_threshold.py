import logging
from typing import Any, Callable, Dict, Optional, Tuple

import numpy as np
import pytorch_lightning as pl
import torch
from pytorch_lightning.callbacks.callback import Callback
from pytorch_lightning.utilities.exceptions import MisconfigurationException
from torch import Tensor

from sudokuAI.sudoku_lightning.config import PerformanceThresholdCallbackConfig

logger = logging.getLogger(__name__)


class PerformanceThresholds(Callback):

    mode_dict = {"min": torch.lt, "max": torch.gt}

    def __init__(
        self,
        monitor: str,
        thresholds: Dict[int, float],
        verbose: bool = True,
        mode: str = "min",
        log_rank_zero_only: bool = False,
    ):
        super().__init__()
        self.monitor = monitor
        self.thresholds = {th["epoch"]: th["score"] for th in thresholds}
        self.verbose = verbose
        self.mode = mode
        self.wait_count = 0
        self.stopped_epoch = 0
        self.log_rank_zero_only = log_rank_zero_only

        if self.mode not in self.mode_dict:
            raise MisconfigurationException(
                f"`mode` can be {', '.join(self.mode_dict.keys())}, got {self.mode}"
            )

        torch_inf = torch.tensor(np.inf)
        self.inf_score = torch_inf if self.monitor_op == torch.lt else -torch_inf
        self.best_score = self.inf_score

    @staticmethod
    def from_config(
        config: PerformanceThresholdCallbackConfig,
    ) -> "PerformanceThresholds":
        return PerformanceThresholds(
            monitor=config.monitor,
            thresholds=config.thresholds,
            verbose=config.verbose,
            mode=config.mode,
            log_rank_zero_only=config.log_rank_zero_only,
        )

    @property
    def monitor_op(self) -> Callable:
        return self.mode_dict[self.mode]

    def state_dict(self) -> Dict[str, Any]:
        return {
            "stopped_epoch": self.stopped_epoch,
            "thresholds": self.thresholds,
            "best_score": self.best_score,
        }

    def load_state_dict(self, state_dict: Dict[str, Any]) -> None:
        self.stopped_epoch = state_dict["stopped_epoch"]
        self.best_score = (
            state_dict["best_score"] if "best_score" in state_dict else self.inf_score
        )
        self.thresholds = state_dict["thresholds"]

    def _should_skip_check(self, trainer: "pl.Trainer") -> bool:
        from pytorch_lightning.trainer.states import TrainerFn

        return trainer.state.fn != TrainerFn.FITTING or trainer.sanity_checking

    def on_validation_end(
        self, trainer: "pl.Trainer", pl_module: "pl.LightningModule"
    ) -> None:
        if self._should_skip_check(trainer):
            return
        self._run_early_stopping_check(trainer)

    def _run_early_stopping_check(self, trainer: "pl.Trainer") -> None:
        """Checks whether the early stopping condition is met and if so tells
        the trainer to stop the training."""
        logs = trainer.callback_metrics

        if trainer.fast_dev_run:  # short circuit if metric not present
            return

        current_score = logs[self.monitor].squeeze()
        current_epoch = trainer.current_epoch
        should_stop, reason = self._evaluate_stopping_criteria(
            current_score=current_score, current_epoch=current_epoch
        )

        # stop every ddp process if any world process decides to stop
        should_stop = trainer.strategy.reduce_boolean_decision(should_stop)
        trainer.should_stop = trainer.should_stop or should_stop
        if should_stop:
            self.stopped_epoch = trainer.current_epoch
        if reason and self.verbose:
            self._log_info(trainer, reason, self.log_rank_zero_only)

    def _evaluate_stopping_criteria(
        self, current_score: Tensor, current_epoch: int
    ) -> Tuple[bool, Optional[str]]:
        should_stop = False
        reason = None

        if self.monitor_op(current_score, self.best_score.to(current_score.device)):
            reason = self._improvement_message(current_score)
            self.best_score = current_score

        for epoch, score_threshold in self.thresholds.items():
            if current_epoch >= epoch and self.monitor_op(
                -self.best_score, -score_threshold
            ):
                should_stop = True
                reason = (
                    f"Threshold score {score_threshold} by {epoch} not reached, "
                    f"best score {self.best_score}"
                )
                break
        return should_stop, reason

    def _improvement_message(self, current: Tensor) -> str:
        """Formats a log message that informs the user
        about animprovement in the monitored score."""
        if torch.isfinite(self.best_score):
            msg = (
                f"Metric {self.monitor} improved by "
                f"{abs(self.best_score - current):.3f} >="
                f" New best score: {current:.3f}"
            )
        else:
            msg = f"Metric {self.monitor} improved. New best score: {current:.3f}"
        return msg

    @staticmethod
    def _log_info(
        trainer: Optional["pl.Trainer"], message: str, log_rank_zero_only: bool
    ) -> None:
        if trainer:
            # ignore logging in non-zero ranks if log_rank_zero_only flag is enabled
            if log_rank_zero_only and trainer.global_rank != 0:
                return
            # if world size is more than one then specify the rank of the
            # process being logged
            if trainer.world_size > 1:
                logger.info(f"[rank: {trainer.global_rank}] {message}")
                return

        # if above conditions don't meet and we have to log
        logger.info(message)
