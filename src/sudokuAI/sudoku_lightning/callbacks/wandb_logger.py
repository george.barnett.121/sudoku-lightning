"""Basic Training script"""

# Basic Imports
import logging
from typing import Any

# External Imports
import pytorch_lightning as pl

import wandb

# Internal Imports
# n/a

# Global Variables
_logger = logging.getLogger(__name__)


class FinishableWandbLogger(pl.loggers.WandbLogger):
    def __init__(self, *args: Any, mode: str = "online", **kwargs: Any):
        wandb.init(mode=mode)
        super().__init__(*args, **kwargs)

    def finalize(self, *args, **kwargs):
        self.finish()
        super().finalize(*args, **kwargs)

    @staticmethod
    def finish():
        _logger.info("Finishing wandb...")
        wandb.finish()
