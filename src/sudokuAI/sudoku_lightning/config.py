# Copyright George Barnett 2020

"""Configs
"""

from pathlib import Path

# Basic Imports
from typing import Dict, List, Literal, Optional, Sequence, Union

# External Imports
from pydantic import BaseModel

# Internal Imports


class DefaultConfig(BaseModel):
    class Config:
        extra = "forbid"
        frozen = True

    def json(self, *args, **kwargs):
        return self.model_dump_json(*args, **kwargs)


class ModelConfig(DefaultConfig):
    name: Literal["Model"] = "Model"


class LinearConfig(DefaultConfig):
    name: Literal["Linear"] = "Linear"
    features: Sequence[int] = [81, 324, 324, 81]
    batch_norm: bool = True
    skip: bool = True
    nonlinearity: Literal[
        "none",
        "relu",
        "relu6",
        "sigmoid",
        "tanh",
        "prelu",
    ] = "tanh"


class LinearClassifierConfig(LinearConfig):
    name: Literal["LinearClassifier"] = "LinearClassifier"
    features: Sequence[int] = [81, 324, 324, 81 * 10]
    num_classes: int = 10


class LossConfig(DefaultConfig):
    name: Literal["CrossEntropy", "L1", "L2", "MSE"] = "CrossEntropy"


class DatasetConfig(DefaultConfig):
    """Configuration for datasets during training

    Note this is for both datasets, not one per dataset"""

    train_path: Path = "./dataset/processed/train.hdf5"
    val_path: Path = "./dataset/processed/val.hdf5"
    test_path: Optional[Path] = "./dataset/processed/val.hdf5"
    workers: None | int = 3
    persistent_workers: bool = True
    cache: bool = True
    batch_size: int = 10000


class OptimizerConfig(DefaultConfig):
    optimizer_name: Literal["Adam"] = "Adam"
    lr_scheduler: Literal["ReduceOnPlateau", "WarmupCosine"] = "ReduceOnPlateau"


class LitModuleConfig(DefaultConfig):
    model: Union[LinearConfig, LinearClassifierConfig] = LinearClassifierConfig()
    loss: LossConfig = LossConfig()
    optimizer: OptimizerConfig = OptimizerConfig()
    lr: float = 1e-4
    normalization: bool = True
    normalization_factor: float = 10


class _LoggerBaseConfig(DefaultConfig):
    name: str


class TensorBoardLoggerConfig(_LoggerBaseConfig):
    name: Literal["tb", "tensorboard"] = "tb"
    log_graph: bool = False


class WandbLoggerConfig(_LoggerBaseConfig):
    name: Literal["wandb"] = "wandb"
    log_model: bool = True
    mode: str = "online"


class CSVLoggerConfig(_LoggerBaseConfig):
    name: Literal["csv"] = "csv"


LoggerConfig = Union[TensorBoardLoggerConfig, WandbLoggerConfig, CSVLoggerConfig]


class _CallbackBaseConfig(DefaultConfig):
    name: str


class LearningRateMonitorConfig(_CallbackBaseConfig):
    name: Literal["LearningRateMonitorCallback"] = "LearningRateMonitorCallback"
    logging_interval: Literal["step", "epoch"] = "step"
    log_momentum: bool = True


class PerformanceThresholdCallbackConfig(_CallbackBaseConfig):
    name: Literal["PerformanceThresholdCallback"] = "PerformanceThresholdCallback"
    monitor: Optional[str] = None  # If None, we use the default from training
    thresholds: List[Dict[Literal["epoch", "score"], float | int]] = [
        {"epoch": 3, "score": 0.1},
        {"epoch": 10, "score": 0.3},
        {"epoch": 25, "score": 0.5},
        {"epoch": 75, "score": 0.75},
    ]
    verbose: bool = True
    mode: Literal["min", "max"] = "min"
    log_rank_zero_only: bool = True


class StochasticWeightAveragingConfig(_CallbackBaseConfig):
    name: Literal["StochasticWeightAveragingCallback"] = (
        "StochasticWeightAveragingCallback"
    )
    swa_lrs: Optional[float] = None


class EarlyStoppingConfig(_CallbackBaseConfig):
    name: Literal["EarlyStoppingCallback"] = "EarlyStoppingCallback"
    monitor: None | str = None
    mode: None | Literal["min", "max"] = None
    patience: int = 10
    check_finite: bool = True
    min_delta: float = 0.001
    strict: bool = (False,)


CallbackConfig = Union[
    PerformanceThresholdCallbackConfig,
    LearningRateMonitorConfig,
    StochasticWeightAveragingConfig,
    EarlyStoppingConfig,
]


class TrainerConfig(DefaultConfig):
    max_epochs: int = 100
    accelerator: str = "auto"
    devices: Union[int, Sequence[int]] = 1
    precision: int = 16
    detect_anomaly: bool = False
    deterministic: bool = False
    gradient_clip_val: Union[int, float, None] = 1.0
    profiler: Optional[str] = "pytorch"

    # Allow extras as it is passed to Trainer whicha re loaded for training
    # Simply set defaults here
    class Config:
        extra = "allow"


class ExpConfig(DefaultConfig):
    exp_name: Optional[str] = "default"
    version: Optional[str] = None
    logdir: Path = "./logs/"
    litmodule: LitModuleConfig = LitModuleConfig()
    dataset: DatasetConfig = DatasetConfig()
    trainer: TrainerConfig = TrainerConfig()
    loggers: List[LoggerConfig] = [WandbLoggerConfig()]
    callbacks: List[CallbackConfig] = [
        PerformanceThresholdCallbackConfig(),
        LearningRateMonitorConfig(),
        StochasticWeightAveragingConfig(),
        EarlyStoppingConfig(),
    ]
    verbose: bool = True
    debug: bool = False
