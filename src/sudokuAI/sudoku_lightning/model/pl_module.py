# Copyright George Barnett 2020

"""Models for Training
"""

# Basic Imports

# External Imports
from typing import Literal, Optional

import pytorch_lightning as pl
from torch import Tensor, nn
from torch.optim import Adam, lr_scheduler
from torchmetrics import Accuracy

# Internal Imports
from ..config import (
    LinearClassifierConfig,
    LinearConfig,
    LitModuleConfig,
    LossConfig,
    ModelConfig,
)
from ..losses import CrossEntropyLoss
from .linear import BasicLinear, Linear2dClassifer


class LitLinearSudoku(pl.LightningModule):

    # Record the main training loss and validation metric
    train_loss_str = "train/loss"
    val_metric_str = "val/acc"
    val_metric_direction: Literal["min", "max"] = "max"

    NORMALIZATION_FACTOR = 10

    # --- Actual Model --- #
    def __init__(self, config: Optional[LitModuleConfig] = None):
        super().__init__()
        if config is None:
            config = LitModuleConfig()
        # Merge model_config and loss_config dict
        self._config = config
        self.save_hyperparameters(config.dict())
        self.lr = config.lr
        self.loss_func = self._configure_loss(config.loss)
        self.model = self._configure_model(config.model)
        self.save_hyperparameters(
            {"parameters": sum(param.numel() for param in self.parameters())}
        )

        self.train_acc = Accuracy()
        self.val_acc = Accuracy()

    def normalize(self, x: Tensor):
        return x / self._config.normalization_factor

    def forward(self, x):
        if self._config.normalization:
            x = self.normalize(x)
        return self.model(x)

    # --- Training Configuration --- #
    def training_step(self, batch, batch_idx):
        x, y_target = batch
        y_pred, y_hat = self(x)
        loss = self.loss_func(y_hat, y_target)
        self.log("train/loss", loss)
        self.log("train/loss_bn_avg", loss / y_target.shape[0], on_epoch=True)
        acc = self.train_acc(y_pred, y_target.long())
        self.log("train/acc", self.train_acc, on_step=False, on_epoch=True)
        return {"loss": loss, "acc": acc}

    def validation_step(self, batch, batch_idx):
        x, y_target = batch
        y_pred, y_hat = self(x)
        loss = self.loss_func(y_hat, y_target)
        self.log("val/loss", loss)
        self.log("val/loss_bn_avg", loss / y_target.shape[0], on_epoch=True)

        acc = self.val_acc(y_pred, y_target.long())
        self.log("val/acc", self.val_acc, on_step=False, on_epoch=True)
        return {"loss": loss, "acc": acc}

    def test_step(self, batch, batch_idx):
        x, y_target = batch
        y_pred, y_hat = self(x)
        loss = self.loss_func(y_pred, y_target)
        self.log("test/loss", loss)
        return {"loss": loss}

    def on_train_epoch_end(self) -> None:
        # This doesn't feel like the best way, but it works to record the hp_metric
        self.log("hp_metric", self.trainer.callback_metrics[self.val_metric_str])
        # self.trainer.logger.log_metrics(
        #     {"hp_metric": self.trainer.callback_metrics[self.val_metric_str]},
        # )
        return super().on_train_end()

    def configure_optimizers(self):
        config = self._config.optimizer
        if config.optimizer_name != "Adam":
            raise NotImplementedError("Must ust Adam optimizer")
        optimizer = Adam(self.parameters(), lr=self.lr, weight_decay=1e-3)
        if config.lr_scheduler == "ReduceOnPlateau":
            scheduler = lr_scheduler.ReduceLROnPlateau(
                optimizer,
                threshold=0.001,
                threshold_mode="rel",
                mode=self.val_metric_direction,
                factor=0.1,
                patience=9,
                cooldown=1,
                verbose=True,
            )
        elif config.lr_scheduler == "WarmupCosine":
            scheduler = lr_scheduler.ChainedScheduler(
                [
                    lr_scheduler.LinearLR(
                        optimizer,
                        start_factor=1e-8,
                        end_factor=1.0,
                        total_iters=int(self.trainer.max_epochs * 0.05),
                    ),
                    lr_scheduler.CosineAnnealingWarmRestarts(
                        optimizer,
                        T_0=int(self.trainer.max_epochs),
                        T_mult=1,
                        eta_min=1e-6,
                    ),
                ]
            )
        else:
            raise NotImplementedError("Not implemented LR Scheduler")

        return {
            "optimizer": optimizer,
            "lr_scheduler": {
                "scheduler": scheduler,
                "interval": "epoch",
                "monitor": self.val_metric_str,
            },
        }

    def _configure_loss(self, config: LossConfig) -> nn.Module:
        """Configure losses

        Not a pytorch lightning default callback, must be called
        """
        loss_funcs = {
            "L1": nn.L1Loss,
            "L2": nn.MSELoss,
            "MSE": nn.MSELoss,
            "CrossEntropy": CrossEntropyLoss,
        }
        if config.name not in loss_funcs:
            raise ValueError(
                f"Loss function {config.name} not supported."
                f"Choose from {loss_funcs.keys()}"
            )

        return loss_funcs[config.name]()

    def _configure_model(self, config: ModelConfig) -> nn.Module:
        """Configure model

        Not a pytorch lightning default callback, must be called
        """
        if type(config) is LinearClassifierConfig:
            return Linear2dClassifer.from_cfg(config)
        elif type(config) is LinearConfig:
            return BasicLinear.from_cfg(config)
        else:
            raise ValueError(f"Invalid config type {type(config)}")
