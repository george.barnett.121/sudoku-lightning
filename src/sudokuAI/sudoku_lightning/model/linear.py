# Copyright George Barnett 2020

"""Model functions

"""

import logging

# Basic Imports
from abc import ABC
from typing import List, Optional, Tuple

# External Imports
from torch import LongTensor, Tensor, nn
from torch.nn import functional as F

# Internal Imports
from sudokuAI.sudoku_lightning.config import LinearClassifierConfig, LinearConfig

# Global Variables
logger = logging.getLogger(__name__)


class _AbstractModel(nn.Module, ABC):
    def forward(self, x) -> Tuple[LongTensor, Tensor]: ...


_non_linearity_lookup = {
    "none": nn.Identity,
    "relu": nn.ReLU,
    "relu6": nn.ReLU6,
    "sigmoid": nn.Sigmoid,
    "tanh": nn.Tanh,
    "prelu": nn.PReLU,
}


class LinearBlock(nn.Module):
    def __init__(
        self,
        num_features_in: int,
        num_features_out: int,
        batchnorm: bool = True,
        skip: bool = False,
        activation: Optional[str] = "none",
    ):
        super(LinearBlock, self).__init__()
        self.skip = skip
        self.batchnorm = batchnorm
        self.layer = nn.Linear(num_features_in, num_features_out, bias=True)
        if activation is None:
            activation = "none"
        self.activation = _non_linearity_lookup[activation]()
        self.bn = nn.BatchNorm1d(num_features=num_features_out) if batchnorm else None

    def forward(self, x):
        if self.batchnorm and (x.shape[0] == 1):
            raise ValueError(
                "Cannot use BatchNorm1d with a batch size of 1. See "
                "https://github.com/pytorch/pytorch/issues/7716. "
                "Please set the batch size to <= 2"
            )
        x_in = x.clone()
        x = self.layer(x)

        x = self.activation(x)
        if self.bn:
            x = self.bn(x)

        if self.skip:
            if x.shape[-1] == x_in.shape[-1]:
                pass
            elif x.shape[-1] < x_in.shape[-1]:
                # Cut the x_in values to match x
                x_in = x_in[..., : x.shape[-1]]
            else:
                # Pad the end of x_in to match x
                x_in = F.pad(x_in, (0, x.shape[-1] - x_in.shape[-1]))
            x = x + x_in

        return x


class BasicLinear(nn.Module):
    @classmethod
    def from_cfg(cls, config: LinearConfig):
        return cls(config.features, config.skip, config.batch_norm, config.nonlinearity)

    def __init__(
        self,
        num_features: List[int] = None,
        skip: bool = True,
        batch_norm=True,
        nonlinearity: str = "relu",
    ) -> None:
        super(BasicLinear, self).__init__()
        if num_features is None:
            num_features = [81, 81]
        if not len(num_features):
            raise ValueError("Cannot have zero layers")

        body_list = []
        # Get each conesecutive pair of num_features
        # except last pair, which we do separately
        for in_feature, out_feature in zip(num_features[:-2], num_features[1:-1]):
            body_list.append(
                LinearBlock(
                    in_feature,
                    out_feature,
                    skip=skip,
                    batchnorm=batch_norm,
                    activation=nonlinearity,
                )
            )

        self.body = nn.Sequential(*body_list)
        # Handle final linear, no batchnorm or relu
        self.head = LinearBlock(
            num_features[-2],
            num_features[-1],
            skip=skip,
            batchnorm=False,
            activation="none",
        )

    def forward(self, x: Tensor) -> Tensor:
        x = x / 10
        x = x.view(-1, 81)
        x = self.body(x)
        x = self.head(x)
        x = x.view(-1, 9, 9)
        x = x * 10
        return x.round().long().clamp(min=0, max=9), x


class Linear2dClassifer(BasicLinear):
    @classmethod
    def from_cfg(cls, config: LinearClassifierConfig):
        return cls(
            config.features,
            config.skip,
            config.batch_norm,
            config.nonlinearity,
            config.num_classes,
        )

    def __init__(
        self,
        num_features: List[int] = None,
        skip: bool = True,
        batch_norm=True,
        nonlinearity: str = "relu",
        num_classes: int = 10,
    ) -> None:
        if num_features is None:
            num_features = [81, 81 * 10]
        super().__init__(
            num_features=num_features,
            skip=skip,
            batch_norm=batch_norm,
            nonlinearity=nonlinearity,
        )
        self.num_classes = num_classes
        self.head = LinearBlock(
            num_features[-2],
            num_features[-1],
            skip=False,
            batchnorm=False,
            activation="none",
        )

    def forward(self, x: Tensor) -> Tensor:
        x_ = x.view(-1, 81)
        x_ = self.body(x_)
        x_ = self.head(x_)
        y_ = x_.view(-1, self.num_classes, 9, 9)
        # y_ = F.softmax(y_, dim=1)
        return y_.argmax(dim=-3), y_
