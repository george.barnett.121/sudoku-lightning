# Copyright George Barnett 2020

"""Process raw data

Process raw dataset into train, val, and test split. Save the raw data into 2
arrays, problems and solutions. Accepts either a ZIP file or individual CSV files.
"""

import argparse
import csv
import logging
import random
import zipfile

# Basic Imports
from pathlib import Path
from typing import Any, Dict, List, Tuple

import h5py

# External Imports
import numpy as np

# Internal Imports

# dataset is assumed from here:
# https://www.kaggle.com/bryanpark/sudoku

# Global Variables
logger = logging.getLogger(__name__)


def main(
    input_zip: Path = None,
    input_csv: Path = None,
    output_dir: Path = None,
    seed=None,
    split_proportions: Dict[str, float] = None,
):
    if split_proportions is None:
        split_proportions = {"train": 0.8, "test": 0.2}
    seed = set_random_seed(seed)
    board_size = (9, 9)

    # If ZIP is provided, unzip and find the CSV
    if input_zip:
        logger.info(f"Extracting {input_zip}")
        input_csv = unzip_archive(input_zip, output_dir)

    # Ensure the CSV file is provided
    if not input_csv:
        raise ValueError("Either a ZIP file or a CSV file must be provided.")

    logger.info("Started processing raw data")
    all_problems, all_solutions = read_csv(input_csv)
    logger.info(f"Read {len(all_problems)} puzzles.")

    idxs = list(range(len(all_problems)))
    idx_splits = split_data(idxs, list(split_proportions.values()))

    logger.info(f"Split data into: {[len(s) for s in idx_splits]}")
    logger.info(f"Converting to {board_size} arrays")
    data_split = {}
    for split_name, idxs in zip(split_proportions.keys(), idx_splits):
        split_problems = [convert_str_to_arr(all_problems[i], board_size) for i in idxs]
        split_solutions = [
            convert_str_to_arr(all_solutions[i], board_size) for i in idxs
        ]
        data_split[split_name] = {
            "problems": split_problems,
            "solutions": split_solutions,
        }

    logger.info("Saving data")

    for tag, data in data_split.items():
        save_path = output_dir / f"{tag}.hdf5"
        output_dir.mkdir(exist_ok=True, parents=True)
        save_hdf5(data, save_path)
        logger.info(f"Saved {tag} data")


def set_random_seed(seed: int = None):
    if seed is None:
        seed = random.randint(0, 1000)
    random.seed(seed)
    return seed


def split_data(list_to_split: List[Any], proportions: List[float]):
    if abs(1 - sum(proportions)) > 0.001:
        raise ValueError("Proportions to split must add to 1")
    if len(proportions) == 1:
        return [list_to_split]
    a_split, b_split = a_b_random_sample(list_to_split, proportions[0])

    # Further split b with other proportions
    # scale other proportions for remaining data
    adjusted_proportions = [p / sum(proportions[1:]) for p in proportions[1:]]
    b_parts = split_data(b_split, adjusted_proportions)
    output = [a_split]
    for part in b_parts:
        output.append(part)
    return output


def convert_str_to_arr(
    board_str: str,
    board_size: Tuple[int, int] = (9, 9),
) -> np.ndarray:
    """Convert string board representation to array board

    Convert a board represented as concatenated ints into a numpy array
    representing the board. The string is converted into rows then columns.

    Args:
        board_str: string representing the array

    Returns: numpy array version of the board
    """
    # convert int to list of int digits
    list_arr = [int(digit) for digit in board_str]
    return np.array(list_arr, dtype=np.int8).reshape(board_size)


def read_csv(csv_path: Path) -> Tuple[List[str], List[str]]:
    """Reads problems and solutions from a single CSV file."""
    problems = []
    solutions = []

    with open(csv_path, "r") as file:
        csv_reader = csv.reader(file)
        headers = next(csv_reader)
        if headers != ["quizzes", "solutions"]:
            raise AttributeError("CSV does not match expected format.")
        for quiz, sol in csv_reader:
            problems.append(quiz)
            solutions.append(sol)

    return problems, solutions


def unzip_archive(zip_path: Path, output_dir: Path) -> Path:
    """Unzips the archive and returns the path to the extracted CSV file."""
    with zipfile.ZipFile(zip_path, "r") as zip_ref:
        zip_ref.extractall(output_dir)

    # Assuming the CSV file is named 'sudoku.csv'
    csv_file = output_dir / "sudoku.csv"

    if not csv_file.exists():
        raise FileNotFoundError("Expected CSV file not found in the ZIP archive")

    return csv_file


def save_hdf5(data: np.ndarray, save_path: Path):
    with h5py.File(save_path, "w") as hf:
        for key, val in data.items():
            hf.create_dataset(key, data=val)


def a_b_random_sample(data: List[Any], proportion: float):
    a_size = int(proportion * len(data))
    # Use sets of idxs so we can subtract them
    all_idxs = set(range(len(data)))
    a_idxs = random.sample(list(all_idxs), a_size)
    b_idxs = list(all_idxs - set(a_idxs))
    a_data = [data[idx] for idx in a_idxs]
    b_data = [data[idx] for idx in b_idxs]
    return a_data, b_data


def cli():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-i",
        "--input-zip",
        default="dataset/raw/archive.zip",
        type=Path,
        help="Path to input ZIP file containing CSVs.",
    )
    parser.add_argument(
        "-c",
        "--input-csv",
        default=None,
        type=Path,
        help="Path to the CSV file. Required if ZIP is not provided.",
    )
    parser.add_argument(
        "-o",
        "--output-dir",
        default="dataset/processed/",
        type=Path,
        help="Output directory to save processed data. Must be a directory"
        "though it does not have to exist",
    )
    parser.add_argument(
        "-r",
        "--random-seed",
        default=None,
        type=int,
        help="Random seed for CSV sampling",
    )
    parser.add_argument(
        "-p",
        "--proportions",
        default=[0.8, 0.1, 0.1],
        nargs="+",
        type=float,
        help="The number and proportion to split the data. It will round down"
        "to the first number. Must match the number of tags in -t",
    )
    parser.add_argument(
        "-t",
        "--tags",
        default=["train", "val", "test"],
        nargs="+",
        type=str,
        help="Tags to apply to each dataset split. Number must match the proportions",
    )

    args = parser.parse_args()
    logging.basicConfig(level=logging.DEBUG)

    # Ensure either the ZIP file or CSV file is provided
    if not args.input_zip and not args.input_csv:
        raise ValueError("Either a ZIP file or a CSV file must be provided.")

    if len(args.proportions) != len(args.tags):
        raise AttributeError("Number of proportions must match number of tags")

    splits = {t: p for p, t in zip(args.proportions, args.tags)}
    main(
        input_zip=args.input_zip,
        input_csv=args.input_csv,
        output_dir=args.output_dir,
        seed=args.random_seed,
        split_proportions=splits,
    )


if __name__ == "__main__":
    cli()
