"""Metrics for measuring performance

These should be structured similar to losses
"""

# Basic Imports
import logging

# External Imports
import torch
from torch import Tensor, nn

# Internal Imports
# n/a

# Global Variables
_logger = logging.getLogger(__name__)


def _reduce(val, mode="mean"):
    _valid_modes = ["mean", "sum", "none"]
    if mode == "mean":
        return val.float().mean()
    elif mode == "sum":
        return val.long().sum()
    elif mode == "none":
        return val
    else:
        raise ValueError(f"Invalid reduction mode, choose from {_valid_modes}")


class Accuracy(nn.Module):
    """Measure class accuracy"""

    def __init__(self, reduction="mean") -> None:
        self.reduction = reduction

        super().__init__()

    def forward(self, input: Tensor, target: Tensor) -> Tensor:
        if input.dtype == torch.float:
            input = torch.round(input)
        if target.dtype != torch.long:
            raise ValueError("Invalid target type, must be torch.long")
        accurate = input == target
        return _reduce(accurate, self.reduction)
