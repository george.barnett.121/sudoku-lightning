# Sudoku Lightning

Warning: personal project! This is really for me to try out some random tools and create a real working example of a ML system that isn't CIFAR 10 and classication.

Problem:
Train simple MLP to solve sodoku.

## Train models

## Setup


### Python environment
This project uses `pre-commit``.
```

pip install -e .[all]

pre-commit
```


### Dataset
If you have just pulled the repository directly, you can just run `sudoku-preprocess`. If you need to change directories or anything else, check with `sudoku-preprocess --help`.


### Run Training

To train example model (not meant for good performance!):
```
python src/sudokuAI/sudoku_lightning/train.py
--exp-yamls \
config/classifier.yaml \
--overload \
/"trainer.fast_dev_run=1 \
trainer.gpus=1 \
dataset.batch_size=100 \
dataset.cache=True \
dataset.workers=4 \
litmodule.model.features=[81,324,324,810] \
litmodule.model.nonlinearity=tanh \
litmodule.model.skip=True \
litmodule.model.batch_norm=True \
trainer.max_epochs=3 \
trainer.limit_train_batches=2 \
trainer.limit_val_batches=1 \
trainer.auto_lr_find=False \
trainer.precision=16 \
litmodule.lr=1e-2
```


To run hp_search:
`python src/sudokuAI/sudoku_lightning/hp_search.py`

### Implemented tools:

* Pytorch Lightning
* HyperOpt
* Gitlab CI
* Pydantic config parsing
* OmegaConf (cli and yaml overriding)
* Pyscaffold
    * pre-commit
    * setup.cfg
    * linting/black
    * namespace
