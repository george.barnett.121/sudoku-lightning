import math

from hyperopt import hp

space = {
    "exp_name": "hp_debug",
    "num_trials": 3,
    "yaml_overload": ["config/debug.yaml"],
    "kwarg_overload": {
        "trainer": {
            "max_epochs": 5,
            "precision": hp.choice("precision", [16, 32]),
        },
        "litmodule": {
            "lr": hp.loguniform("lr", math.log(5e-3), math.log(5e-2)),
            "optimizer": {
                "lr_scheduler": hp.choice(
                    "lr_scheduler", ["ReduceOnPlateau", "WarmupCosine"]
                )
            },
            "normalization": hp.choice("normalization", [True, False]),
            "model": hp.choice(
                "model_type",
                [
                    {
                        "name": "LinearClassifier",
                        "features": hp.choice(
                            "features",
                            [
                                tuple([81] + [810] * 1 + [81 * 10]),
                                tuple([81] + [810] * 3 + [81 * 10]),
                            ],
                        ),
                        "nonlinearity": "relu",
                        "skip": True,
                        "batch_norm": True,
                    },
                ],
            ),
        },
        "dataset": {
            "batch_size": hp.qloguniform(
                "batch_size", math.log(500), math.log(10000), 1
            )
        },
    },
}
