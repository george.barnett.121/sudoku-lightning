import math

from hyperopt import hp

space = {
    "exp_name": "transformer_classifier_initial_search",
    "num_trials": 50,
    "yaml_overload": ["config/transformer_classifier.yaml"],
    "kwarg_overload": {
        "trainer": {
            "max_epochs": hp.choice("max_epochs", [100, 200, 300]),
            "precision": hp.choice("precision", [16, 32]),
        },
        "litmodule": {
            "lr": hp.loguniform("lr", math.log(1e-4), math.log(1e-2)),
            "optimizer": {
                "lr_scheduler": hp.choice(
                    "lr_scheduler", ["ReduceOnPlateau", "WarmupCosine"]
                )
            },
            "normalization": hp.choice("normalization", [True, False]),
            "model": hp.choice(
                "model_type",
                [
                    {
                        "name": "TransformerEncoderClassifier",
                        "hidden_size": hp.choice("hidden_size", [64, 82, 128]),
                        "nhead": hp.choice("nhead", [2, 4, 8]),
                        "num_layers": hp.choice("num_layers", [2, 4, 6]),
                        "dim_feedforward": hp.choice(
                            "dim_feedforward", [2 * 81, 3 * 81, 4 * 81]
                        ),
                        "dropout": hp.uniform("dropout", 0.1, 0.5),
                        "activation": hp.choice("activation", ["relu", "gelu"]),
                        "layer_norm_eps": hp.loguniform(
                            "layer_norm_eps", math.log(1e-6), math.log(1e-4)
                        ),
                    },
                ],
            ),
        },
        "dataset": {
            "batch_size": hp.qloguniform("batch_size", math.log(32), math.log(10000), 1)
        },
    },
}
