import math

from hyperopt import hp

space = {
    "exp_name": "linear_classifier_recreate_best_2",
    "num_trials": 20,
    "yaml_overload": ["config/debug.yaml"],
    "kwarg_overload": {
        "trainer": {
            "max_epochs": 200,
            "precision": 32,
        },
        "litmodule": {
            "lr": hp.loguniform("lr", math.log(5e-3), math.log(5e-2)),
            "optimizer": {
                "lr_scheduler": hp.choice(
                    "lr_scheduler", ["ReduceOnPlateau", "WarmupCosine"]
                )
            },
            "normalization": hp.choice("normalization", [True, False]),
            "model": hp.choice(
                "model_type",
                [
                    {
                        "name": "LinearClassifier",
                        "features": hp.choice(
                            "features",
                            [
                                tuple([81] + [810] * 10 + [81 * 10]),
                                tuple([81] + [810] * 25 + [81 * 10]),
                            ],
                        ),
                        "nonlinearity": "relu",
                        "skip": True,
                        "batch_norm": True,
                    },
                ],
            ),
        },
        "dataset": {
            "batch_size": hp.qloguniform(
                "batch_size", math.log(500), math.log(10000), 1
            )
        },
    },
}
