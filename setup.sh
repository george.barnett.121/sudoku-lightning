#!/bin/bash

set -x -e

# Install requirements
pip install -e .[all]

# Enable pre-commit hooks
pre-commit install

# Run sudoku-preprocess
sudoku-preprocess
